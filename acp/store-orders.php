<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
                </li>

            </ol>
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fad fa-receipt"></i> Store Orders (orders placed on <i class="fab fa-paypal"></i> PayPal) </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-dark" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Payment ID</th>
                                <th>Payer ID</th>
                                <th>Payer Email</th>
                                <th>Amount</th>
                                <th>Order status</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Order ID</th>
                                <th>Payment ID</th>
                                <th>Payer ID</th>
                                <th>Payer Email</th>
                                <th>Amount</th>
                                <th>Order status</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php
                            $paypal_orders = $mysqliA->query("SELECT * FROM `payments`") or die (mysqli_error($mysqliA));
                            $num_query = $paypal_orders->num_rows;
                            if($num_query < 1)
                            {
                                echo '<tr><td colspan="6">There are no orders yet</td></tr>';
                            }
                            else
                            {
                                while($res = $paypal_orders->fetch_assoc())
                                {
                                    $order_id = $res['id'];
                                    $payment_id = $res['payment_id'];
                                    $payer_id = $res['payer_id'];
                                    $payer_email = $res['payer_email'];
                                    $amount = $res['amount'];
                                    $currency = $res['currency'];
                                    $payment_status = $res['payment_status'];

                                    switch ($payment_status) {
                                        case "approved":
                                            $class = 'success';
                                            break;
                                        case "failed":
                                            $class = 'warning';
                                            break;
                                        case "created":
                                            $class = 'primary';
                                            break;
                                    }

                                    echo '
                                    <tr>
                                        <td>'.  $order_id .'</td>
                                        <td>'.  $payment_id .'</td>
                                        <td>'.  $payer_id .'</td>
                                        <td>'.  $payer_email .'</td>
                                        <td>'.  $amount .' '. $currency .'</td>
                                        <td><span class="badge badge-'. $class .'"> '.  strtoupper($payment_status) .'</span></td>
                                    </tr>
                                    ';
                                }
                            }

                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
<?php
include ('footer.php');
?>