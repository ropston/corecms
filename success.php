<?php /** @noinspection HtmlUnknownTarget */
include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-check-circle"></i> Pruchase status!</h2>
                        <?php
                        if(isset($_SESSION['id']))
                        {
                            $bnetID = $_SESSION['id'];
                            $get_accID = $mysqliA->query("SELECT * FROM `account` WHERE `battlenet_account` = '$bnetID';") or die (mysqli_error($mysqliA));
                            while($bnet_ress = $get_accID->fetch_assoc())
                            {
                                $accountID = $bnet_ress['id'];
                            }
                            // Once the transaction has been approved, we need to complete it.
                            if (array_key_exists('paymentId', $_GET) && array_key_exists('PayerID', $_GET)) {
                                $transaction = $gateway->completePurchase(array(
                                    'payer_id'             => $_GET['PayerID'],
                                    'transactionReference' => $_GET['paymentId'],
                                ));
                                $response = $transaction->send();

                                if ($response->isSuccessful())
                                {
                                    // The customer has successfully paid.
                                    $arr_body = $response->getData();

                                    $payment_id = $arr_body['id'];
                                    $payer_id = $arr_body['payer']['payer_info']['payer_id'];
                                    $payer_email = $arr_body['payer']['payer_info']['email'];
                                    $amount = $arr_body['transactions'][0]['amount']['total'];
                                    $currency = PAYPAL_CURRENCY;
                                    $payment_status = $arr_body['state'];

                                    // Insert transaction data into the database
                                    $isPaymentExist = $mysqliA->query("SELECT * FROM `payments` WHERE `payment_id` = '$payment_id'");
                                    $check_payment = $isPaymentExist->num_rows;
                                    if($check_payment < 1)
                                    {
                                        $insert = $mysqliA->query("INSERT INTO `payments` (`payment_id`, `account_id`, `payer_id`, `payer_email`, `amount`, `currency`, `payment_status`, `coins_claimed`) VALUES('$payment_id', '$accountID', '$payer_id', '$payer_email', '$amount', '$currency', '$payment_status', '0');") or die (mysqli_error($mysqliA));
                                        if($insert === true)
                                        {
                                            echo '
                                                <div class="alert alert-success">
                                                    <i class="fad fa-check-circle"></i> Purchase complete! <br />
                                                    Transaction id: '. $payment_id . ' <br />
                                                    Go to <a href="'.$custdir.'/ucp.php">UCP</a> to claim your coins!
                                                </div>
                                            ';
                                            header("refresh:5; url=$custdir/ucp.php");
                                        }
                                        else
                                        {
                                            echo '
                                                <div class="alert alert-warning">
                                                    <i class="fad fa-exclamation-triangle"></i> Error payment! Duplicated transaction!
                                                </div>
                                            ';
                                            header("refresh:3; url=$custdir/ucp.php");
                                        }
                                    }
                                }
                                else
                                {
                                    echo $response->getMessage();
                                }
                            }
                            else
                            {
                                echo '
                                    <div class="alert alert-warning">
                                        <i class="fad fa-exclamation-triangle"></i> Transaction is declined!
                                    </div>
                                ';
                                header("refresh:3; url=$custdir/ucp.php");
                            }
                        }
                        else
                        {
                            echo '
                                <div class="alert alert-info">
                                    <i class="fad fa-exclamation-circle"></i> You need to be logged in to buy coins!
                                </div>
                            ';
                            header("refresh:3; url=$custdir/login.php");
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>